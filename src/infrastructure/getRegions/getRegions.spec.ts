import axios, { AxiosResponse } from "axios";
import getRegions from "./getRegions";
import { Region } from "./Region";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

const data: Region[] = [
  { nom: "Bretagne", code: "53" },
  { nom: "Corse", code: "94" },
];

const createAxiosResponse = (
  regions: Region[],
  status: number,
  statusText: string
): AxiosResponse<Region[]> => ({
  data: regions,
  status: status,
  statusText: statusText,
  config: {},
  headers: {},
});

describe("Test unitaire du service des regions", () => {
  describe("Récupération de toutes le regions", () => {
    beforeAll(() => {
      mockedAxios.get.mockResolvedValue({ data: data, status: 200 });
    });
    it("Je peux recupérer toutes les regions", async () => {
      const regions: Region[] = await getRegions();
      expect(mockedAxios.get).toHaveBeenCalled();
      expect(regions).toEqual(data);
    });
  });
});
