type Region = {
  nom: string;
  code: string;
};

export type { Region };
