import axios, { AxiosResponse } from "axios";
import { Region } from "./Region";

const pathApi: string = "https://geo.api.gouv.fr";
const pathRegion: string = "regions";

const getRegions = async (): Promise<Region[]> => {
  try {
    const response: AxiosResponse<Region[]> = await axios.get(
      `${pathApi}/${pathRegion}`
    );
    if (response.status >= 400 && response.status < 500) {
      throw new Error("Erreur Fonctionnelle");
    }
    if (response.status >= 500) {
      throw new Error("Erreur Technique");
    }
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
};

export default getRegions;
export { pathApi, pathRegion };
