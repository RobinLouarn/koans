import getRegions from "./getRegions";
import { Region } from "./Region";

describe("Test d'integration du service des regions", () => {
  describe("Récupération de toutes le regions", () => {
    it("Je peux recupérer toutes les regions", async () => {
      const regions: Region[] = await getRegions();
      expect(regions.length).toBeGreaterThanOrEqual(0);
    });
  });
});
