import { AppState } from "../store/appStore";

type RootStackParamList = {
  Profile: { appState: AppState };
  Search: { appState: AppState };
};

export type { RootStackParamList };
