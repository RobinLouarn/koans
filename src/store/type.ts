type Event<S, P> = {
  action: Action<S, P>;
  payload: P;
};

interface Action<S, P> {
  (oldState: S, payload: P): S;
}

interface Reducer<S, P, E extends Event<S, P>> {
  (oldState: S, envent: E): S;
}

export type { Reducer, Action, Event };
