import { Event, Reducer, Action } from "./type";
import { Region } from "../infrastructure/getRegions/Region";

type AppState = {
  regions: Region[];
};

const initalAppState: AppState = {
  regions: [],
};

const setRegions: Action<AppState, Region[]> = (
  oldState: AppState,
  payload: Region[]
): AppState => ({
  ...oldState,
  regions: payload,
});

const appReducer: Reducer<AppState, any, Event<AppState, any>> = (
  state: AppState,
  { action, payload }: Event<AppState, any>
): AppState => action(state, payload);

export default appReducer;
export { setRegions, initalAppState };
export type { AppState };
