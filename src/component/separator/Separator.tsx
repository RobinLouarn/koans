import React from "react";
import styled from "styled-components/native";
import { FC, ReactElement } from "react";

type SeparatorProps = { color: string };

const HeadbandWrapper = styled.View`
  justify-content: center;
  align-items: center;
  height: 50px;
  background-color: #79a292;
`;

const SeparatorWrapper = styled.Text<{ color: string }>`
  ${({ color }) => `border: 2px ${color};`}
  border-radius: 10px;
  height: 0px;
`;

const Separator: FC<SeparatorProps> = ({ color }): ReactElement => (
  <SeparatorWrapper color={color} />
);

export default Separator;
