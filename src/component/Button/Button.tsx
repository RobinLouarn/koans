import React from "react";
import styled from "styled-components/native";
import { FC, ReactElement } from "react";

type NormalButtonProps = {
  label: string;
  labelColor: string;
  buttonColor: string;
  buttonWidth: string;
  buttonHeight: string;
  buttonRadius: string;
  onPress: Function;
};

const ButtonWrapper = styled.TouchableOpacity<{
  buttonColor: string;
  buttonWidth: string;
  buttonHeight: string;
  buttonRadius: string;
}>`
  ${({ buttonColor }) => `background-color : ${buttonColor};`}
  ${({ buttonWidth }) => `width : ${buttonWidth};`}
  ${({ buttonHeight }) => `height : ${buttonHeight};`}
  ${({ buttonRadius }) => `border-radius : ${buttonRadius};`}
  justify-content: center;
  align-items: center;
`;

const TextWrapper = styled.Text<{ labelColor: string }>`
  ${({ labelColor }) => `color: ${labelColor};`}
  border-radius: 15px;
`;

const Button: FC<NormalButtonProps> = ({
  label,
  labelColor,
  buttonColor,
  buttonWidth,
  buttonHeight,
  buttonRadius,
  onPress,
}): ReactElement => (
  <ButtonWrapper
    buttonColor={buttonColor}
    buttonWidth={buttonWidth}
    buttonHeight={buttonHeight}
    buttonRadius={buttonRadius}
    onPress={() => {
      onPress();
    }}
  >
    <TextWrapper labelColor={labelColor}>{label}</TextWrapper>
  </ButtonWrapper>
);

export default Button;
