import React from "react";
import styled from "styled-components/native";
import { FC, ReactElement } from "react";

type HeadbandProps = {
  title: string;
};

const HeadbandWrapper = styled.View`
  justify-content: center;
  align-items: center;
  height: 50px;
  background-color: #79a292;
`;

const TextWrapper = styled.Text`
  font-weight: bold;
`;

const Headband: FC<HeadbandProps> = ({ title }): ReactElement => (
  <HeadbandWrapper>
    <TextWrapper>{title}</TextWrapper>
  </HeadbandWrapper>
);

export default Headband;
