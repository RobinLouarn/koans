import React from "react";
import styled from "styled-components/native";
import { FC, ReactElement } from "react";

type FieldProps = {
  label: string;
  value: string;
};

const FieldWrapper = styled.View`
  flex-direction: row;
`;

const TextWrapper = styled.Text`
  flex: 1;
`;

const Field: FC<FieldProps> = ({ label, value }): ReactElement => (
  <FieldWrapper>
    <TextWrapper>{label} : </TextWrapper>
    <TextWrapper>{value}</TextWrapper>
  </FieldWrapper>
);

export default Field;
