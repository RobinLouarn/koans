import React, { FC, ReactElement, useEffect, useReducer } from "react";
import styled from "styled-components/native";
import { StackScreenProps } from "@react-navigation/stack";
import { RootStackParamList } from "../../navigation/type";
import Button from "../../component/button";
import Headband from "../../component/headband";
import { Region } from "../../infrastructure/getRegions/Region";
import getRegions from "../../infrastructure/getRegions";
import appReducer, { setRegions } from "../../store/appStore";

type SearchProps = StackScreenProps<RootStackParamList, "Search">;

const SearchWrapper = styled.SafeAreaView`
  flex: 1;
  padding: 5px;
`;

const Search: FC<SearchProps> = ({ navigation, route }): ReactElement => {
  const [appState, appDispatch] = useReducer(appReducer, route.params.appState);

  useEffect(() => {
    const wrapper = async () => {
      const regions: Region[] = await getRegions();
      appDispatch({
        action: setRegions,
        payload: regions,
      });
    };
    wrapper();
  }, []);

  return (
    <SearchWrapper>
      <Headband title="Search" />
      <Button
        label="Éditer"
        labelColor="#000000"
        buttonColor="#c3965b"
        buttonHeight="70px"
        buttonWidth="70px"
        buttonRadius="15px"
        onPress={() => {
          navigation.navigate("Profile", { appState });
        }}
      />
    </SearchWrapper>
  );
};

export default Search;
