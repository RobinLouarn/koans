import React, { FC, ReactElement } from "react";
import styled from "styled-components/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { RootStackParamList } from "../../../navigation/type";
import Button from "../../../component/button";
import Field from "../../../component/field";
import { AppState } from "../../../store/appStore";
type ProfileNavigationProp = StackNavigationProp<RootStackParamList, "Profile">;

type HeaderProps = {
  navigation: ProfileNavigationProp;
  appState: AppState;
};

const HeaderWrapper = styled.View`
  flex-direction: column;
  padding: 5px;
`;

const HeaderLine = styled.View`
  flex-direction: row;
  padding: 5px;
`;

const HeaderElement = styled.View<{ alignItems: string }>`
  ${({ alignItems }) => `justifyContent: ${alignItems};`}
  flex: 1;
  padding: 5px;
`;

const HeaderTitle = styled.Text`
  font-weight: bold;
`;

const ProfileHeader: FC<HeaderProps> = ({
  navigation,
  appState,
}): ReactElement => (
  <HeaderWrapper>
    <HeaderLine>
      <HeaderElement alignItems="center">
        <HeaderTitle>Mon profil : </HeaderTitle>
      </HeaderElement>
      <HeaderElement alignItems="flex-end">
        <Button
          label="Editer"
          labelColor="#000000"
          buttonColor="#c3965b"
          buttonHeight="70px"
          buttonWidth="70px"
          buttonRadius="50px"
          onPress={() => {
            navigation.navigate("Search", { appState });
          }}
        />
      </HeaderElement>
    </HeaderLine>
    <HeaderLine>
      <HeaderElement alignItems="flex-end">
        <Field label="Test1" value="Value1" />
      </HeaderElement>
      <HeaderElement alignItems="flex-end">
        <Field label="Test2" value="Value2" />
      </HeaderElement>
    </HeaderLine>
    <HeaderLine>
      <HeaderElement alignItems="flex-end">
        <Field label="Test3" value="Value3" />
      </HeaderElement>
      <HeaderElement alignItems="flex-end">
        <Field label="Test4" value="Value4" />
      </HeaderElement>
    </HeaderLine>
  </HeaderWrapper>
);

export default ProfileHeader;
