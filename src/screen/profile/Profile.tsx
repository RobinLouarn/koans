import React, { FC, ReactElement, useEffect, useReducer } from "react";
import styled from "styled-components/native";
import { StackScreenProps } from "@react-navigation/stack";
import { RootStackParamList } from "../../navigation/type";
import Headband from "../../component/headband";
import Separator from "../../component/separator";
import ProfileHeader from "./header";
import ProfileBody from "./body";
import appReducer, { AppState, initalAppState } from "../../store/appStore";

type ProfileProps = StackScreenProps<RootStackParamList, "Profile">;

const ProfileWrapper = styled.SafeAreaView`
  flex: 1;
  padding: 5px;
`;

const Profile: FC<ProfileProps> = ({ navigation }): ReactElement => {
  const [appSate, dispatch] = useReducer(appReducer, initalAppState);

  return (
    <ProfileWrapper>
      <Headband title="Profile" />
      <ProfileHeader navigation={navigation} appState={appSate} />
      <Separator color="black" />
      <ProfileBody />
    </ProfileWrapper>
  );
};

export default Profile;
