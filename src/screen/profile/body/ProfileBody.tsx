import React, { FC, ReactElement } from "react";
import styled from "styled-components/native";
import { Text } from "react-native";
import Button from "../../../component/button";

type ProfileHeaderProps = {};

const ProfileHeaderWrapper = styled.View`
  padding: 5px;
`;

const BodyLine = styled.View`
  flex-direction: row;
  padding: 5px;
`;

const BodyElement = styled.View`
  flex: 1;
  padding: 5px;
  justify-content: flex-start;
  justify-content: center;
`;

const ProfileHeader: FC<ProfileHeaderProps> = (): ReactElement => (
  <ProfileHeaderWrapper>
    <BodyLine>
      <BodyElement>
        <Button
          label="Annuaire"
          labelColor="#ffffff"
          buttonColor="#263f3e"
          buttonHeight="70px"
          buttonWidth="120px"
          buttonRadius="15px"
          onPress={() => {
            console.log("Annuaire");
          }}
        />
      </BodyElement>
      <BodyElement>
        <Text>Rechecher une école</Text>
      </BodyElement>
    </BodyLine>
    <BodyLine>
      <BodyElement>
        <Button
          label="Carnet de bord"
          labelColor="#000000"
          buttonColor="#79a292"
          buttonHeight="70px"
          buttonWidth="120px"
          buttonRadius="15px"
          onPress={() => {
            console.log("Carnet de bord");
          }}
        />
      </BodyElement>
      <BodyElement>
        <Text>Rentrer un remplacement</Text>
      </BodyElement>
    </BodyLine>
    <BodyLine>
      <BodyElement>
        <Button
          label="Récap"
          labelColor="#000000"
          buttonColor="#c3965b"
          buttonHeight="70px"
          buttonWidth="120px"
          buttonRadius="15px"
          onPress={() => {
            console.log("Récap");
          }}
        />
      </BodyElement>
      <BodyElement>
        <Text>Suivre ses déplacements, ses heures, et ses kms</Text>
      </BodyElement>
    </BodyLine>
  </ProfileHeaderWrapper>
);

export default ProfileHeader;
