# Projet Brigande

## User Story 1 - récupération des données

Pour les points suivants, les fonctions seront dans une même classe `EcolesFetcher` dans le fichier `EcolesFetcher.js`.

N'oubliez pas les tests unitaires ainsi que la JSDoc.

### 1. fetchAcademies

Ecrire une fonction `fetchAcademies` qui appelle le endpoint `http://localhost:8080/api/departements` et enregistre le résultat dans l'objet courant.

La fonction enregistre :

- une liste d'académies si elles sont trouvées
- un tableau vide en cas de status 404

Elle renvoie :

- une exception contenant le message d'erreur dans tous les autres cas

### 2. fetchDepartements

Ecrire une fonction `fetchDepartements` qui prend en paramètre le code d'une académie et appelle le endpoint `http://localhost:8080/api/departements` et enregistre le résultat dans l'objet courant.

La fonction enregistre :

- une liste de départements si ils sont trouvés
- un tableau vide en cas de status 404

Elle renvoie :

- une exception contenant le message d'erreur dans tous les autres cas

### 3. fetchCirconscriptions

Ecrire une fonction `fetchCirconscriptions` qui prend en paramètre le code d'un département et appelle le endpoint `http://localhost:8080/api/circonscriptions` et enregistre le résultat dans l'objet courant.

La fonction enregistre :

- une liste de circonscriptions si ils sont trouvés
- un tableau vide en cas de status 404

Elle renvoie :

- une exception contenant le message d'erreur dans tous les autres cas

### 4. fetchEcoles

Ecrire une fonction `fetchEcoles` qui prend en paramètre le code d'une circonscription et appelle le endpoint `http://localhost:8080/api/ecoles` et enregistre le résultat dans l'objet courant.

La fonction renvoie :

- une liste d'écoles si elles sont trouvées
- un tableau vide en cas de status 404

Elle renvoie :

- une exception contenant le message d'erreur dans tous les autres cas

2.  Ecrire les tests unitaires correspondants

## User Story 2 - Premier écran

1.  Créer un écran `Welcome.js` dans le répertoire `/Projet`
2.  Ecrire un component affichant 'Bienvenue sur Brigande'
3.  Ouvrir le fichier `App.js` et importer votre nouveau component de façon à l'afficher dans le component `<SafeAreaView>`

## User Story 3 - Un écran stylé

Ouvrir le fichier `Brigande-Accueil.png` et modifier l'écran `Welcome.js` pour correspondre à l'image (voir l'annexe pour les références ce style)
L'écran est composé des éléments suivants :

- un bandeau affichant "ACCUEIL"
- une partie "Mon profil" possédant les éléments suivants :
  - sur la première ligne, le titre "mon profil :" aligné à gauche, et un bouton "modifier" aligné à droite
  - sur la deuxième ligne divisée en 2 colonnes, le texte "Académie de Rennes" dans la première colonne et le texte "Finistère" dans la deuxième colonne
  - sur la troisième ligne divisée en 2 colonnes, le texte "Brest Est" dans la première colonne et le texte "Goarem Gozh, Plougastel" dans la deuxième colonne
- une ligne de séparation
- la partie "actions" possédant les éléments suivants :
  - sur la première ligne, le titre "Mes actions"
  - sur la deuxième ligne divisée en 2 colonnes, le bouton "Annuaire" dans la première colonne et le texte "rechercher une école" dans la deuxième colonne
  - sur la troisième ligne divisée en 2 colonnes, le bouton "Carnet de bord" dans la première colonne et le texte "rentrer un remplacement" dans la deuxième colonne
  - sur la quatrième ligne divisée en 2 colonnes, le bouton "Récap" dans la première colonne et le texte "suivre ses remplacements, ses heures et ses Kms" dans la deuxième colonne

Annexes :

1. Bandeau d'accueil
   - hauteur : 50
   - couleur de fond : #79a292
2. Bouton `modifier` :
   - hauteur : 70
   - largeur :70
   - forme ronde (radius = hauteur / 2)
   - couleur : #c3965b
3. Bouton `Annuaire` :
   - hauteur : 70
   - bords arrondis (radius 15)
   - couleur de fond : #263f3e
   - couleur de texte : #ffffff
4. Bouton `Carnet de bord` :
   - hauteur : 70
   - bords arrondis (radius 15)
   - couleur de fond : #79a292
   - couleur de texte : #000000
5. Bouton `Récap` :
   - hauteur : 70
   - bords arrondis (radius 15)
   - couleur de fond : #c3965b
   - couleur de texte : #000000

## User Story 4 - Découpage du component

Le component `Welcome` est long et difficile à comprendre. De plus, il contient du code dupliqué. Il faut le découper en components réutilisables.

## User Story 5 - Fetch une donnée

### 1. Créer l'écran Profil

Ouvrir le fichier `Brigande-Profil.png` et créer l'écran `Profil.js` pour correspondre à l'image (voir l'annexe pour les références ce style)

L'écran est composé des éléments suivants :

- un bandeau affichant "PROFIL"
- une section "Académie" composée de 2 lignes :
  - sur la première ligne, le libellé "Académie" aligné à gauche
  - sur la deuxième ligne, une liste déroulante centrée
- une section "Département" composée de 2 lignes :
  - sur la première ligne, le libellé "Département" aligné à gauche
  - sur la deuxième ligne, une liste déroulante centrée
- une section "Circonscription" composée de 2 lignes :
  - sur la première ligne, le libellé "Circonscription" aligné à gauche
  - sur la deuxième ligne, une liste déroulante centrée
- une section "Ecole de rattachement" composée de 2 lignes :
  - sur la première ligne, le libellé "Ecole de rattachement" aligné à gauche
  - sur la deuxième ligne, une liste déroulante centrée
- un bouton "démarrer" centré horizontalement

Pour la liste déroulante, utiliser le component "DropDownPicker" (voir annexe).

### 2. Reprendre les fetchers dans EcolesFetcher

Reprendre les fonctions de `EcolesFetcher` pour les utiliser dans le component et remplir les listes déroulantes. Les données doivent être reformatées pour correspondre au format de `DropDownPicker` (value, label).

Le mapping des données est le suivant pour chaque élément :

- value => code
- label => nom

### 3. Appeler les fonctions pour remplir les listes déroulantes

### Annexes :

1. Bandeau "PROFIL"
   - hauteur : 50
   - couleur de fond : #79a292
2. Bouton `Récap` :
   - hauteur : 50
   - bords arrondis (radius 10)
   - couleur de fond : #c3965b
   - couleur de texte : #000000
3. La liste déroulante
   Nous allons utiliser le component "DropDownPicker" pour la liste. Voici un exemple d'implémentation :

   ```
   import DropDownPicker from 'react-native-dropdown-picker';

   ...

   export default class XXX extends Component {
      render() {
         return (

            ...

            <DropDownPicker
               items={listeItems}
               defaultValue={defaultValue}
               placeholder="Un texte par défaut"
               onChangeItem={callback}
               containerStyle={{ height: 50 }}
            />

            ...

         )
      }
   }
   ```

   Notes :

   - listeItems est un tableau d'élements de la forme `{value: string, label: string}`
   - defaultValue doit être le champ `value` de l'élément que l'on veut sélectionner par défaut
   - la callback est la fonction qui sera appelée quand on sélectionne un élément. Elle permet de générer des effets de bord et prend en paramètre un item de la forme `{value: string, label: string}`
   - il peut y avoir des problèmes de liste déroulante apparaîssant derrière les autres composants au lieu d'apparaître devant. Il faut dans ce cas utiliser la propriété `zIndex` dans le style du conteneur parent de DropDownPicker.

## User Story 6 - Ajout d'un loader

En tant qu'utilisateur, lorsqu'une donnée est en cours de chargement (par exemple la liste des académies), je souhaite qu'un loader apparaîsse.

## User Story 7 - Navigation

En tant qu'utilisateur, lorsque j'appuie sur le bouton `démarrer` de l'écran `Profil`, je veux être redirigé vers l'écran `Accueil`.
Depuis l'écran `Accueil`, lors du clic sur `modifier`, je veux retourner sur l'écran `Profil`. Les listes déroulantes sont déjà chargées, et les éléments sont présélectionnés conformément à ma dernière saisie.

### Annexe (pour plus tard, n'allez pas trop vite bande de chenapans):

Installer `React-Navigation`
Se référer au Getting-Started de React-Navigation :

```
https://reactnavigation.org/docs/getting-started
```

## User Story 8 - Partager les données

En tant qu'utilisateur, lorsque j'arrive sur l'écran `Accueil`, je veux que les données que j'ai sélectionné dans l'écran `Profil` soit affichées.

## User Story 9 - Sauvegarder les données

En tant qu'utilisateur, je souhaite que l'école de rattachement soit sauvegardée dans le téléphone.

Lorsque l'utilisateur ouvre l'application, si il a déjà rentré son école de rattachement, il ne doit pas avoir à les resaisir.

## User Story 10 - Components fonctionnels

Transformer les components classe en components fonctionnels. Commencer par les components les plus simples (ceux qui n'utilisent pas de HOC ou qui n'utilisent pas de fonctions de cycle de vie).
